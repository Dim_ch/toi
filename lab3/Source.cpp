﻿#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include <locale>
#include <strsafe.h>

#include "Header.h"

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "");

    TCHAR curDir[255];
    DWORD res = 0;
    res = GetCurrentDirectory(255, curDir);

    if (res == 0) return 0;

    TCHAR filePath[310];
    HRESULT result = StringCchPrintf(filePath, 310, TEXT("%s\\%s"), curDir, TEXT("title.txt"));

    if (S_OK != result) return 0;

    std::ifstream file(filePath);

    if (file.is_open()) {

        std::string Title;
        std::vector<std::string> Titles;
        std::vector<std::vector<std::string>> BasisWords;

        int i = 0;
        while (!file.eof()) {
            getline(file, Title);
            Titles.push_back(Title);
            BasisWords.push_back(ParseTitle(Title));
            if (++i == 9) break;
        }

        size_t countTitle = BasisWords.size();
        std::vector<WORDINFO> table = AnalizeWords(BasisWords);
        std::vector<std::string> words;

        alglib::real_2d_array A = GetFrequencyMatrix(table, countTitle, &words);
        alglib::ae_int_t m = A.rows(), n = A.cols();
        alglib::real_1d_array W;
        alglib::real_2d_array U, Vt;
        alglib::rmatrixsvd(A, m, n, 2, 2, 2, W, U, Vt);

        PrintFrequencyMat(A, words);
        PrintSVDMatr(W, U, Vt);

        std::vector<NODE*>* clusters = Clastering(Vt);

        file.close();

        HRESULT result = StringCchPrintf(filePath, 310, TEXT("%s\\%s"), curDir, TEXT("Cluster_Title.txt"));

        if (S_OK != result) return 0;

        std::ofstream fileW(filePath, std::ios::trunc);

        if (fileW.is_open()) {
            for (size_t i = 0; i < clusters->size(); ++i) {
                fileW << "============\n";
                fileW << "Кластер " << i << ":\n\n";
                std::vector<MYPOINT> points;
                GetClusterPoints(clusters->at(i), &points);

                for (size_t j = 0; j < points.size(); ++j) {
                    fileW << Titles[points[j].title] << "\n";
                }

                fileW << "\n";
            }

            fileW.close();
        }
    }
    return 0;
}
