﻿#pragma once
#include <string>

using namespace std;

// Стоп-символы
extern string STOP_SYMBOLS[];
// Стоп-слова
extern string STOP_WORDS[];
extern size_t STOP_WORDS_LEN;

// Словарь гласных символов русского алфавита
extern char VOWEL[];
// Длина массива с глассными символами
extern size_t VOWLen;

// Словарь согласных символов русского алфавита
extern char CONSONANT[];
// Длина массива с согласными символами
extern size_t CONLen;

// Русский алфавит
extern string ALPAHBET;

// МАССИВЫ ОКОНЧАНИЯ СЛОВ
extern string ADJECTIVE[];
extern string PARTICIPLE[];
extern string REFLEXIVE[];
extern string VERB[];
extern string NOUN[];
extern string SUPERLATIVE[];
extern string DERIVATIONAL[];

// ДЛИНЫ МАССИВОВ С ОКОНЧАНИЯМИ СЛОВ
extern size_t PEGELen;
extern size_t ADJLen;
extern size_t PARLen;
extern size_t REFLen;
extern size_t VERLen;
extern size_t NOUNLen;
extern size_t SUPLen;
extern size_t DERLen;

// Отделяет область строки word после совпадения символа из этой строки с элементом массива dict.
// Область подстроки возвращается в wordArea.
// Функция возвращает true при успешном поиске и false в противном случае.
bool GetWordArea(const string word, string* wordArea, const char* dict, const size_t* dictLen);

// Отделяет область строки word после совпадения одного символа из этой строки
// с элементом массива dict1 и последующего символа с элементом массива dict2.
// Область подстроки возвращается в wordArea.
// Функция возвращает true при успешном поиске и false в противном случае.
bool GetWordArea2(const string word, string* wordArea, const char* dict1, const size_t* dict1Len,
    const char* dict2, const size_t* dict2Len);

// Ищет окончание слова в строке word, которое совпадет с самым длинным окончанием из массива окончаний dict.
// Возвращает true, если окончание было найдено, в переменную id записывается индекс символа в строке word
// с которого начинается окончание. Если окончание не было найдено функция возвращает false.
bool FindEndWord(const string word, const string* dict, const size_t* dictLen, size_t* id);

// Выполняет поиск основы слова по алгоритму Стеммера Портера.
// Основа слова записывается в base. Возвращает true при успешной работе,
// иначе - false.
bool Stemmer(const string word, string* base);
