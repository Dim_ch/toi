﻿#pragma once
#include "Header.h";

struct MYPOINT {
	double x;
	double y;
	int    title;

	bool operator == (const MYPOINT& point) {
		return point.x == this->x && point.y == this->y && point.title == this->title;
	}
};

struct NODE {
	NODE*    left;
	NODE*    right;
	MYPOINT* point;

	bool operator == (const NODE &node) {
		return node.point == this->point && node.left == this->left && node.right == this->right;
	}
};

// Выполняет кластеризацию. Возвращает vector с кластерами.
std::vector<NODE*>* Clastering(alglib::real_2d_array);

// Объединяет кластеры, пока их станет не больше 3
// Выделяет память под NODE*. Количество элементов в nodes должно быть не меньше 3.
void GetClusters(std::vector<NODE*>* nodes);

// Получает среднее расстояние между кластерами (метод средней связи)
double GetAVGDistanceClusters(NODE*, NODE*);

// Получает вектор MYPOINT из поддерева (кластера)
void   GetClusterPoints(NODE*, std::vector<MYPOINT>*);

// Получает расстояние между 2-мя точками
double GetDistance(MYPOINT*, MYPOINT*);

// создаёт копию node (выделяет память)
NODE* DuplicateNode(NODE* node);