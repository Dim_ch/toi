﻿#include "Header.h"
#include "Stemming.h"

// Подсчитывает количество вхождений слова в вектор
WORDINFO NumberOfWords(std::string word, std::vector<std::string> arr, int iTitle) {
    WORDINFO wordInfo = { word, 0 , iTitle };
    for (std::string str : arr) {
        if (word == str)
            wordInfo.count++;
    }
    return wordInfo;
}

// Ищет слово, которое встретилось в загаловке
bool FindWordInTable(std::vector<WORDINFO> table, WORDINFO wordInf) {
    for (size_t i = 0; i < table.size(); ++i) {
        if (wordInf.word == table[i].word && wordInf.iTitle == table[i].iTitle)
            return true;
    }
    return false;
}

// Ищет слово, которое встретилось больше 1 раза
bool FindRepeatWordInTable(std::vector<WORDINFO> table, WORDINFO wordInf) {
    int count = wordInf.count;
    for (size_t i = 0; i < table.size(); ++i) {
        if (wordInf.word == table[i].word && wordInf.iTitle != table[i].iTitle)
            count += table[i].count;
    }

    return count > 1;
}

// Возвращает слова, которые встретились больше 1 раза. Записывает количество уникальных слов в countUnique
std::vector<WORDINFO> AnalizeWords(std::vector<std::vector<std::string>> words) {
    std::vector<WORDINFO> tables;
    std::vector<WORDINFO> IndexTable;
    WORDINFO wInfo;
    for (size_t i = 0; i < words.size(); ++i) {
        for (std::string word : words[i]) {
            wInfo = NumberOfWords(word, words[i], i + 1);
            if (!FindWordInTable(tables, wInfo)) {
                tables.push_back(wInfo);
            }
        }
    }

    // Выписать слова, которые встретились больше 1 раза
    for (WORDINFO wInfo : tables) {
        if (FindRepeatWordInTable(tables, wInfo))
            IndexTable.push_back(wInfo);
    }
    return IndexTable;
}

// Разбивает строку на слова по сепаратору
std::vector<std::string> StringSplit(std::string Line, char separator) {
    std::vector<std::string> words;
    // Выделяем слова из строки.
    std::string word = "";
    for (size_t i = 0; i < Line.length(); ++i) {
        if (Line[i] == separator) {
            if (word.length() > 0) {
                words.push_back(word);
                word.clear();
            }
            continue;
        }
        word += Line[i];
    }

    if (word.length() > 0) {
        words.push_back(word);
        word.clear();
    }

    return words;
}

std::string ToLowerCase(std::string word) {
    std::string lowerWord = "";
    for (size_t i = 0; i < word.length(); ++i)
        lowerWord += tolower(word[i]);
    return lowerWord;
}

// Удаляет стоп-символы из word
std::string DeleteStopSymbols(std::string word) {
    std::string new_word = "";
    for (size_t i = 0; i < word.length(); ++i) {
        if (STOP_SYMBOLS->find(word[i]) == -1)
            new_word += word[i];
    }
    return new_word;
}

// Возвращает true, если word стоп-слово
bool IsStopWord(std::string word) {
    if (word.length() == 0)
        return true;

    for (size_t i = 0; i < STOP_WORDS_LEN; ++i) {
        if (word == STOP_WORDS[i])
            return true;
    }

    return false;
}
// Удаляет стоп-слова из вектора и стоп-символы из слов
std::vector<std::string> DeleteStopWords(std::vector<std::string> words) {
    std::vector<std::string> new_words;
    if (!words.empty()) {

        for (size_t i = 0; i < words.size(); ++i) {
            if (!IsStopWord(DeleteStopSymbols(words[i])))
                new_words.push_back(words[i]);
        }
    }
    return new_words;
}

// Выделяет основу слова
std::vector<std::string> SelectBasisWord(std::vector<std::string> words) {
    std::vector<std::string> basisWords;

    if (!words.empty()) {
        std::string word;

        for (size_t i = 0; i < words.size(); ++i) {
            if (Stemmer(words[i], &word))
                if (word.length() > 0)
                    basisWords.push_back(word);
        }
    }
    return basisWords;
}

// Возвращает основы слов из строки
std::vector<std::string> ParseTitle(std::string Line) {
    std::vector<std::string> words = StringSplit(Line, ' ');

    for (size_t i = 0; i < words.size(); ++i)
        words[i] = ToLowerCase(words[i]);

    return SelectBasisWord(DeleteStopWords(words));
}

// Возвращает true, если слово уже есть в векторе table
bool FindWord(std::vector<std::string> table, std::string wordInf) {
    for (size_t i = 0; i < table.size(); ++i) {
        if (wordInf == table[i])
            return true;
    }
    return false;
}

// Определяет сколько раз встретилось слово в каждом заголовке. words заполнен 0
void SetCountWordInTitle(std::vector<int>* arrWords, std::string word, std::vector<WORDINFO> words) {
    for (size_t i = 0; i < words.size(); ++i) {
        if (word == words[i].word)
            (*arrWords)[words[i].iTitle - int(1)] = words[i].count;
    }
}

// Формирует частотную матрицу индексируемых слов
alglib::real_2d_array GetFrequencyMatrix(std::vector<WORDINFO> IndexedWords, int countTitle, std::vector<std::string> * UniqueWords) {
    std::vector<int> WordsInTitle;
    // Получаем уникальные слова
    for (size_t i = 0; i < IndexedWords.size(); ++i) {
        if (!FindWord((*UniqueWords), IndexedWords[i].word))
            UniqueWords->push_back(IndexedWords[i].word);
    }

    alglib::real_2d_array A;
    A.setlength((*UniqueWords).size(), countTitle);

    for (size_t i = 0; i < (*UniqueWords).size(); ++i) {
        // заполнить вектор 0
        WordsInTitle.assign(countTitle, 0);
        SetCountWordInTitle(&WordsInTitle, (*UniqueWords)[i], IndexedWords);

        for (int j = 0; j < countTitle; ++j) {
            A[i][j] = (double)WordsInTitle[j]; // составляем матрицу индексируемых слов
        }
    }
    return A;
}

// Выводит частотную матрицу индексируемых слов
void PrintFrequencyMat(alglib::real_2d_array A, std::vector<std::string> UniqueWords) {
    printf("Частотная матрица индексируемых слов\n%15.10s", "");
    for (int i = 0; i < A.cols(); ++i)
        printf(" T%d", i + 1);
    std::cout << "\n";

    for (int i = 0; i < A.rows(); ++i) {
        std::cout.width(15);
        std::cout << UniqueWords[i];
        for (int j = 0; j < A.cols(); ++j) {
            printf(" %0.0f ", A[i][j]);
        }
        std::cout << "\n";
    }
    std::cout << "\n\n";
}

// Вывод сингулярного разложения частотной матрицы индексируемых слов
void PrintSVDMatr(alglib::real_1d_array W, alglib::real_2d_array U, alglib::real_2d_array Vt) {
    std::cout << "U:\n";
    for (int i = 0; i < U.rows(); ++i) {
        for (int j = 0; j < U.cols(); ++j) {
            std::cout.width(10);
            std::cout.precision(3);
            std::cout << U[i][j] << " ";
        }
        std::cout << "\n";
    }

    std::cout << "\nW:\n";
    for (int i = 0; i < W.length(); ++i) {
        std::cout.precision(3);
        std::cout << W[i] << " ";
    }
    std::cout << "\n";

    std::cout << "\nVt:\n";
    for (int i = 0; i < Vt.rows(); ++i) {
        for (int j = 0; j < Vt.cols(); ++j) {
            std::cout.precision(3);
            std::cout.width(10);
            std::cout << Vt[i][j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n\n";
}