﻿// В данном файле содержаться функции и глобальные переменные, необходимые для реализации
// алгоритма Стеммера Портера.
#include <string>

// Заголовоный файл, объявляющий функции и глобальные переменные данного файла
#include "Stemming.h"

using namespace std;

//Стоп-символы и стоп-слова
string STOP_SYMBOLS[] = { "0123456789\",./«»#;:@~[]{}=-+)(*&^%$<>?!\\" };

string STOP_WORDS[] = { "-", "еще", "него", "сказать", "а", "ж", "нее", "со", "без", "же",
    "ней", "совсем", "более", "жизнь", "нельзя", "так", "больше", "за", "нет", "такой", "будет",
    "зачем", "ни", "там", "будто", "здесь", "нибудь", "тебя", "бы", "и", "никогда", "тем", "был",
    "из", "ним", "теперь", "была", "из-за", "них", "то", "были", "или", "ничего", "тогда", "было",
    "им", "но", "того", "быть", "иногда", "ну", "тоже", "в", "их", "о", "только", "вам", "к", "об",
    "том", "вас", "кажется", "один", "тот", "вдруг", "как", "он", "три", "ведь", "какая", "она",
    "тут", "во", "какой", "они", "ты", "вот", "когда", "опять", "у", "впрочем", "конечно", "от",
    "уж", "все", "которого", "перед", "уже", "всегда", "которые", "по", "хорошо", "всего", "кто",
    "под", "хоть", "всех", "куда", "после", "чего", "всю", "ли", "потом", "человек", "вы", "лучше",
    "потому", "чем", "г", "между", "почти", "через", "где", "меня", "при", "что", "говорил", "мне",
    "про", "чтоб", "да", "много", "раз", "чтобы", "даже", "может", "разве", "чуть", "два", "можно",
    "с", "эти", "для", "мой", "сам", "этого", "до", "моя", "свое", "этой", "другой", "мы", "свою",
    "этом", "его", "на", "себе", "этот", "ее", "над", "себя", "эту", "ей", "надо", "сегодня", "я",
    "ему", "наконец", "сейчас", "если", "нас", "сказал", "есть", "не", "сказала", "сказала" };
size_t STOP_WORDS_LEN = sizeof(STOP_WORDS) / sizeof(STOP_WORDS[0]);

char VOWEL[] = { 'а', 'е', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я', 'ё',
                   'А', 'Е', 'И', 'О', 'У', 'Ы', 'Э', 'Ю', 'Я', 'Ё' };
size_t VOWLen = sizeof(VOWEL) / sizeof(VOWEL[0]);

char CONSONANT[] = { 'б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'л', 'м',
                   'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш',
                   'щ', 'Б', 'В', 'Г', 'Д', 'Ж', 'З', 'Й', 'К', 'Л',
                   'М', 'Н', 'П', 'Р', 'С', 'Т', 'Ф', 'Х', 'Ц', 'Ч',
                   'Ш', 'Щ' };
size_t CONLen = sizeof(CONSONANT) / sizeof(CONSONANT[0]);

string ALPAHBET = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

// МАССИВЫ ОКОНЧАНИЯ СЛОВ
string PERFECTIVEGERUND[] = { "ав", "авши", "авшись", "яв", "явши", "явшись",
                              "ив", "ивши", "ившись", "ыв", "ывши", "ывшись" };
string ADJECTIVE[] = { "ее", "ие", "ые", "ое", "ими", "ыми", "ей", "ий",
                       "ый", "ой", "ем", "им",  "ым", "ом", "его", "ого",
                       "ему", "ому", "их", "ых", "ую", "юю", "ая",  "яя",
                        "ою",  "ею" };
string PARTICIPLE[] = { "аем", "яем", "анн", "янн", "авш", "явш", "ающ",
                        "яющ", "ащ", "ящ", "ивш", "ывш", "ующ" };
string REFLEXIVE[] = { "ся", "сь" };
string VERB[] = { "ала", "яла", "ана", "яна", "аете", "яете", "айте", "яйте",
                  "али", "яли", "ай", "яй", "ал", "ял", "аем", "яем", "ан",
                  "ян", "ало", "яло", "ано", "яно", "ает", "яет", "ают",
                  "яют", "аны", "яны", "ать", "ять", "аешь", "яешь", "анно",
                  "янно", "ила", "ыла", "ена", "ейте", "уйте", "ите", "или",
                  "ыли", "ей", "уй", "ил", "ыл", "им", "ым", "ен", "ило",
                  "ыло", "ено", "ят", "ует", "уют", "ит", "ыт", "ены",
                  "ить", "ыть", "ишь", "ую", "ю" };
string NOUN[] = { "а", "ев", "ов", "ие", "ье", "е", "иями", "ями", "ами",
                  "ие", "ии", "и", "ией", "ей", "ой", "ий", "й", "иям",
                  "ям", "ием", "ем", "ам", "ом", "о", "у", "ах", "иях",
                  "ях", "ы", "ь", "ию", "ью", "ю", "ия", "ья", "я" };
string SUPERLATIVE[] = { "ейш", "ейше" };
string DERIVATIONAL[] = { "ост", "ость" };

// ДЛИНЫ МАССИВОВ С ОКОНЧАНИЯМИ СЛОВ;
size_t PEGELen = sizeof(PERFECTIVEGERUND) / sizeof(PERFECTIVEGERUND[0]);
size_t ADJLen = sizeof(ADJECTIVE) / sizeof(ADJECTIVE[0]);
size_t PARLen = sizeof(PARTICIPLE) / sizeof(PARTICIPLE[0]);
size_t REFLen = sizeof(REFLEXIVE) / sizeof(REFLEXIVE[0]);
size_t VERLen = sizeof(VERB) / sizeof(VERB[0]);
size_t NOUNLen = sizeof(NOUN) / sizeof(NOUN[0]);
size_t SUPLen = sizeof(SUPERLATIVE) / sizeof(SUPERLATIVE[0]);
size_t DERLen = sizeof(DERIVATIONAL) / sizeof(DERIVATIONAL[0]);


// Выделяет срез от начала слова word до конца pattern в строку slice.
// Возвращает true при успешной работе, иначе - false.
bool GetPatternSlice(const string word, const string pattern, string* slice)
{
    if (word.length() < pattern.length())
        return false;

    size_t index = word.find(pattern);
    if (string::npos != index) {
        (*slice).clear();
        for (size_t i = 0; i < pattern.size() + index; ++i)
            (*slice).push_back(word[i]); // сформировать исходное слово без отсеченных окончаний
    }
    else
        return false;

    return true;
}


bool GetWordArea(const string word, string* wordArea, const char* dict, const size_t* dictLen)
{
    bool findLetter = false;
    size_t id = 0;

    // поиск индекса начала области
    for (id; word[id] && !findLetter; ++id) {
        // определить гласный символ или нет
        for (size_t j = 0; j < *dictLen; ++j) {
            if (word[id] == dict[j]) {
                findLetter = true;
                break;
            }
        }
    }

    if (findLetter) {
        (*wordArea).clear();
        (*wordArea).append(&word[id]);
    }

    return findLetter;
}


bool GetWordArea2(const string word, string* wordArea, const char* dict1, const size_t* dict1Len,
    const char* dict2, const size_t* dict2Len)
{
    bool response = false;
    size_t id = 0;

    for (id; word[id] && !response; ++id) {
        // определить гласный символ или нет
        for (size_t j = 0; j < *dict1Len && !response; ++j) {
            if (word[id] == dict1[j]) {
                // определить согласный символ или нет
                for (size_t k = 0; word[id + 1] && k < *dict2Len; ++k) {
                    if (word[id + 1] == dict2[k]) {
                        response = true;
                        break;
                    }
                }
            }
        }
    }

    if (response) {
        (*wordArea).clear();
        (*wordArea).append(&word[id + 1]);
    }

    return response;
}


bool FindEndWord(const string word, const string* dict, const size_t* dictLen, size_t* id)
{
    string endWord;
    int tempId = (int)word.length(); // начало окончания
    int size = tempId;
    bool response = false;

    // поиск максимального окончания
    for (int i = size - 1; i >= 0; --i) {
        endWord.clear();
        endWord.append(&word[i]);

        // сравнивать окончание с окончаниями из массива
        for (size_t j = 0; j < *dictLen; ++j) {
            if (endWord.length() == dict[j].length()) {
                // размер окончания больше предыдущего и окончания совпали
                if (tempId > i && endWord == dict[j]) {
                    *id = tempId = i; // сохранить индекс начала окончания
                    response = true;
                }
            }
        }
    }

    return response;
}


bool Stemmer(const string word, string* base)
{
    string RV;

    if (!GetWordArea(word, &RV, VOWEL, &VOWLen))
        return false;

    // ШАГ 1: отделить окончание
    size_t id;
    if (!FindEndWord(RV, PERFECTIVEGERUND, &PEGELen, &id)) {
        if (FindEndWord(RV, REFLEXIVE, &REFLen, &id)) // удалить REFLEXIVE
            RV.resize(id);
        if (FindEndWord(RV, ADJECTIVE, &ADJLen, &id)) { // удалить ADJECTIVE
            RV.resize(id);
            if (FindEndWord(RV, PARTICIPLE, &PARLen, &id)) // удалить PARTICIPLE
                RV.resize(id);
        }
        else if (FindEndWord(RV, VERB, &VERLen, &id)) // удалить VERB
            RV.resize(id);
        else if (FindEndWord(RV, NOUN, &NOUNLen, &id)) // удалить NOUN
            RV.resize(id);
    }
    else
        RV.resize(id);

    // ШАГ 2: если RV оканчивается на И удалить ее
    if (RV.size() > 0 && RV[RV.size() - 1] == 'и')
        RV.resize(RV.size() - 1);

    // ШАГ 3: удалить окончание DERIVATIONAL
    string R1, R2, copy;
    bool findSuccess = false;
    if (GetPatternSlice(word, RV, &copy)) // получить исходное слово без отсеченных окончаний
        if (GetWordArea2(copy, &R1, VOWEL, &VOWLen, CONSONANT, &CONLen))
            if (GetWordArea2(R1, &R2, VOWEL, &VOWLen, CONSONANT, &CONLen))
                if (FindEndWord(R2, DERIVATIONAL, &DERLen, &id))
                {
                    R2.resize(id);
                    findSuccess = true;
                }

    // ШАГ 4
    R1.clear();
    if (findSuccess)
        GetPatternSlice(copy, R2, &R1); // получить исходное слово без отсеченных окончаний
    else
        GetPatternSlice(copy, RV, &R1); // получить исходное слово без отсеченных окончаний
    copy.clear();
    copy.append(R1);

    if (GetWordArea(copy, &RV, VOWEL, &VOWLen)) {
        if (RV.size() > 0)
        {
            if (RV[RV.size() - 1] == 'ь')
               RV.resize(RV.size() - 1);
            else if (RV.size() > 1)
                if (RV[RV.size() - 1] == 'н' && RV[RV.size() - 2] == 'н')
                RV.resize(RV.size() - 1);
            else if (FindEndWord(RV, SUPERLATIVE, &SUPLen, &id))
            {
                RV.resize(id);
                if (RV.size() > 1)
                    if (RV[RV.size() - 1] == 'н' && RV[RV.size() - 2] == 'н')
                        RV.resize(RV.size() - 1);
            }
        }
    }

    // формирование основы слова
    R1.clear();
    if (GetPatternSlice(copy, RV, &R1)) {
        (*base).clear();
        (*base).append(R1);
    }
    else
        return false;

    return true;
}