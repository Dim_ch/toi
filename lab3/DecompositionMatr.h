﻿#pragma once

#include "Header.h"

alglib::real_2d_array GetFrequencyMatrix(std::vector<WORDINFO>, int, std::vector<std::string> *);
void PrintSVDMatr(alglib::real_1d_array, alglib::real_2d_array, alglib::real_2d_array);
void PrintFrequencyMat(alglib::real_2d_array A, std::vector<std::string> UniqueWords);
std::vector<WORDINFO> AnalizeWords(std::vector<std::vector<std::string>> words);
std::vector<std::string> ParseTitle(std::string Line);