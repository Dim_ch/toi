﻿#include "Clastering.h";

std::vector<NODE*>* Clastering(alglib::real_2d_array Vt) {
	int cols = Vt.cols();
	std::vector<NODE>    *nodes  = new std::vector<NODE>;  // кластеры
	std::vector<MYPOINT> *points = new std::vector<MYPOINT>; // точки
	std::vector<NODE*>   *pNodes = new std::vector<NODE*>; // указатели на кластеры

	nodes->resize(cols);
	points->resize(cols);
	pNodes->resize(cols);

	// сотавляем массив точек и кластеров
	for (int j = 0; j < cols; ++j) {
		points->at(j).title = j;
		points->at(j).x     = Vt[0][j]; // 1 строка значения x
		points->at(j).y     = Vt[1][j]; // 2 строка значения y

		nodes->at(j).left  = NULL;
		nodes->at(j).right = NULL;
		nodes->at(j).point = &points->at(j);

		pNodes->at(j) = &nodes->at(j);
	}

	GetClusters(pNodes);

	return pNodes;
}

void GetClusters(std::vector<NODE*>* nodes) {
	if (nodes->size() < 3) return;

	NODE*  cluster1, * cluster2;
	double dist, minDist;

	while (nodes->size() > 3) {
		cluster1 = nodes->at(0);
		cluster2 = nodes->at(1);
		minDist = GetAVGDistanceClusters(cluster1, cluster2);
		// поиск кластеров с минимальным расстоянием между ними
		size_t len = nodes->size() - 1;
		for (size_t i = 0; i < len; ++i) {
			for (size_t j = i; j < len; ++j) {
				dist = GetAVGDistanceClusters(nodes->at(j), nodes->at(j + 1));
				if (dist < minDist) {
					minDist = dist;
					cluster1 = nodes->at(j);
					cluster2 = nodes->at(j + 1);
				}
			}
		}
		// объединение кластеров
		NODE* newNode = new NODE;
		newNode->left  = DuplicateNode(cluster1);
		newNode->right = DuplicateNode(cluster2);
		newNode->point = NULL;

		nodes->erase(std::remove(nodes->begin(), nodes->end(), cluster1), nodes->end());
		nodes->erase(std::remove(nodes->begin(), nodes->end(), cluster2), nodes->end());

		nodes->push_back(newNode);
	}
}

double GetAVGDistanceClusters(NODE* node1, NODE* node2) {
	double                distance = 0;
	std::vector<MYPOINT>* points1 = new std::vector<MYPOINT>();
	std::vector<MYPOINT>* points2 = new std::vector<MYPOINT>();

	GetClusterPoints(node1, points1);
	GetClusterPoints(node2, points2);

	// вычисление среднего расстояние между элементами кластеров
	for (size_t i = 0; i < points1->size(); ++i) {
		for (size_t j = 0; j < points2->size(); ++j) {
			distance += GetDistance(&points1->at(i), &points2->at(j));
		}
	}

	distance /= ((double)points1->size() * (double)points2->size());

	points1->clear();
	points2->clear();
	delete points1;
	delete points2;

	return distance;
}

void GetClusterPoints(NODE* node, std::vector<MYPOINT>* points) {
	// если node это лист дерева
	if (NULL != node->point) points->push_back(*node->point);
	if (node->left)  GetClusterPoints(node->left, points);
	if (node->right)  GetClusterPoints(node->right, points);
}

double GetDistance(MYPOINT* point1, MYPOINT* point2) {
	return sqrt(pow(point2->x - point1->x, 2) + pow(point2->y - point1->y, 2));
}

NODE* DuplicateNode(NODE* node) {
	NODE* copy  = new NODE;
	copy->left  = node->left;
	copy->right = node->right;
	copy->point = node->point;
	return copy;
}