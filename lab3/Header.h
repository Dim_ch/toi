﻿#pragma once
#include <string>
#include <vector>

#include "linalg.h"

struct WORDINFO {
    std::string word;
    int count;
    int iTitle;
};

#include "DecompositionMatr.h"
#include "Clastering.h"