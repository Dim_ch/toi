#include <iostream>
#include <fstream>
#include <string>

using namespace std;

bool FindChar(char c, string str);
int SearchDirect(string s, string subS);
int SearchKnutMorrisPratt(string str, string subS);
int SearchBoyerMoore(string str, string subS);

int main()
{
    setlocale(LC_ALL, "");

    string filePath;

    cout << "������� ������ ���� � ����� � ����� �������: �:\\<�����>\\<����>\n" << endl;
    cout << "���� � �����: ";
    cin >> filePath;

    ifstream file(filePath);

    if (file.is_open())
    {
        int res = -1;
        string s;
        string subS;

        do {
            cout << "������� ������ ��� ��� ������ � �����: ";
            cin >> subS;
            if (subS.length() < 2)
                cout << "��������� ������ �������� ��� ������� �� ���� ��������.\n";
        } while (subS.length() < 2);

        // ������ �� ��� ���, ���� �� ����������� ���� ���� ��� 
        // �� ����� ������ ������ subS
        int i = 0;
        while (res == -1 && getline(file, s))
        {
            res = SearchDirect(s, subS);
            ++i;
        }

        if (res != -1)
        {
            cout << "\n������ ��� ������ � ������ � " << i << endl << s << endl;

            cout << "�������� ������� ������ ����� ������ ������� � ������� " << ++res << endl;

            res = SearchKnutMorrisPratt(s, subS);
            if (res != -1)
                cout << "�������� �����-�������-������ ����� ������ ������� � ������� " << ++res << endl;
            else
                cout << "�������� �����-�������-������ �� ����� ������." << endl;

            res = SearchBoyerMoore(s, subS);
            if (res != -1)
                cout << "�������� ������-���� ����� ������ ������� � ������� " << ++res << endl;
            else
                cout << "�������� ������-���� �� ����� ������." << endl;
        }
        else
            cout << "��������� " << subS << " �� ���� ������� � ����� " << filePath << "." << endl;

        file.close();
    }
    else
        cout << "�������� �������� � ��������� �����." << endl;

    return 0;
}


// ������� ���������, ���� �� ������ c � ������ str.
// ���� ������ ����, ������� ���������� TRUE, ����� - FALSE.
bool FindChar(char c, string str)
{
    for (int i = 0; str[i]; ++i)
        if (str[i] == c)
            return true;
    return false;
}


// ������� ������� ������ ��������� subS � ������ s. ��� �������� ������
// ���������� ������ ������ ��������� ��������� � ������, � ���������
// ������ ���������� -1.
int SearchDirect(string str, string subS)
{
    for (size_t i = 0; str[i]; ++i)
        // ���������� ������� � ����� for
        for (size_t j = 0; subS[j] == str[i + j]; ++j)
            // ���� ����� �� ����� ������
            if (!subS[j + 1])
                return i;
    return -1;
}


// ������� ���� ��������� subS � ������ str �� ��������� �����-�������-����� � ���
// �������� ������ ���������� ������ ������ ��������� ��������� � ������, �
// ��������� ������ ���������� -1.
int SearchKnutMorrisPratt(string str, string SubStr)
{
    size_t SubLen = SubStr.length();
    size_t strLen = str.length();
    size_t* pi = new size_t[SubLen];

    size_t i = 1,
           j = 0;
    pi[0] = 0;
    // ������������ ������� � ������� ��������� ��� ��������� SubStr

    while (SubStr[i]) {
        if (SubStr[i] == SubStr[j] && i < SubLen) {
            pi[i] = j + 1;
            ++j;
            ++i;
        }
        else if (j == 0 && i < SubLen) {
            pi[i] = 0;
            ++i;
        }
        else
            j = pi[j - 1];
    }

    // ����� ��������� SubStr � ������ str
    size_t k = 0, // ��� str
           l = 0; // ��� SubStr

    while (str[k]) {
        if (str[k] == SubStr[l]) {
            ++k;
            ++l;
            if (l == SubLen)
                return k - l;
        }
        else if (l == 0) {
            ++k;
            if (k == strLen)
                break;
        }
        else
            l = pi[l - 1];
    }

    delete[] pi;
    return -1;
}


// ������� ��������� ����� ��������� subS � ������ str �� ��������� ������-����.
// ���� ��������� �������, �� ������� ���������� ������ ������ ���������
// ��������� � ������, � ��������� ������ -1.
int SearchBoyerMoore(string str, string subS)
{
    size_t size = subS.length();
    int* offsets = new int[size];

    int off = 1;
    // ����������� ������� ��������
    for (int i = size - 2; i >= 0; --i, ++off) {
        offsets[i] = off;
        for (int j = size - 2; j > i; --j) {
            if (subS[i] == subS[j]) {
                offsets[i] = offsets[j];
                break;
            }
        }
    }

    for (int i = 0, len = size - 1; i < size; ++i) {
        if (subS[len] == subS[i])
            offsets[len] = offsets[i];
    }
    // ����� ������ � ������
    int pos = 0;
    off = 0;
    while (pos < str.length()) {
        // ��������� ������� �� ���������
        if (subS[size - 1] != str[pos + size - 1]) {
            off = size; // �������� ������, ���� ������� ��� � ������
            for (int j = 0; j < size; ++j) {
                if (subS[j] == str[pos + size - 1]) { // ������ ������ ���� � ������
                    off = offsets[j];
                    break;
                }
            }
            pos += off;
        }
        else {
            for (int i = size - 2; i >= 0; --i) {
                // ���� ������� �� �����, �� �������� �� �������� ���������� ������� �������
                if (subS[i] != str[pos + i]) {
                    pos += offsets[size - 1];
                    break;
                }
                else if (i == 0) {
                    delete[] offsets;
                    return pos;
                }
            }
        }
    }

    delete[] offsets;
    return off;
}