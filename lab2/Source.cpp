#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

// ������� ������� �������� �������� ��������
char VOWEL[] = { '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                   '�', '�', '�', '�', '�', '�', '�', '�', '�', '�' };
// ����� ������� � ��������� ���������
size_t VOWLen = sizeof(VOWEL) / sizeof(VOWEL[0]);
// ������� ��������� �������� �������� ��������
char CONSONANT[] = { '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                   '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                   '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                   '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                   '�', '�' };
// ����� ������� � ���������� ���������
size_t CONLen = sizeof(CONSONANT) / sizeof(CONSONANT[0]);
// ������� �������
string ALPAHBET = "�������������������������������������Ũ��������������������������";

// ������� ��������� ����
string PERFECTIVEGERUND[] = { "��", "����", "������", "��", "����", "������",
                              "��", "����", "������", "��", "����", "������" };
string ADJECTIVE[] = { "��", "��", "��", "��", "���", "���", "��", "��",
                       "��", "��", "��", "��",  "��", "��", "���", "���",
                       "���", "���", "��", "��", "��", "��", "��",  "��",
                        "��",  "��" };
string PARTICIPLE[] = { "��", "��", "��", "��", "�", "���", "���", "���" };
string REFLEXIVE[] = { "��", "��" };
string VERB[] = { "��", "��", "���", "���", "��", "�", "�", "��", "�",
                   "��", "��", "��", "��", "��", "��", "���", "���",
                   "���", "���", "���", "����", "����", "���", "���",
                   "���", "��", "��", "��", "��", "��", "��", "��", "���",
                   "���", "���", "��", "���", "���", "��", "��", "���",
                   "���", "���", "���", "��", "�" };
string NOUN[] = { "�", "��", "��", "��", "��", "�", "����", "���", "���",
                  "��", "��", "�", "���", "��", "��", "��", "�", "���",
                  "��", "���", "��", "��", "��", "�", "�", "��", "���",
                  "��", "�", "�", "��", "��", "�", "��", "��", "�" };
string SUPERLATIVE[] = { "���", "����" };
string DERIVATIONAL[] = { "���", "����" };

// ����� �������� � ����������� ����;
size_t PEGELen = sizeof(PERFECTIVEGERUND) / sizeof(PERFECTIVEGERUND[0]);
size_t ADJLen = sizeof(ADJECTIVE) / sizeof(ADJECTIVE[0]);
size_t PARLen = sizeof(PARTICIPLE) / sizeof(PARTICIPLE[0]);
size_t REFLen = sizeof(REFLEXIVE) / sizeof(REFLEXIVE[0]);
size_t VERLen = sizeof(VERB) / sizeof(VERB[0]);
size_t NOUNLen = sizeof(NOUN) / sizeof(NOUN[0]);
size_t SUPLen = sizeof(SUPERLATIVE) / sizeof(SUPERLATIVE[0]);
size_t DERLen = sizeof(DERIVATIONAL) / sizeof(DERIVATIONAL[0]);


// �������� ���� �� ������ ����� word �� ����� pattern � ������ slice.
// ���������� true ��� �������� ������, ����� - false.
bool GetPatternSlice(const string word, const string pattern, string* slice)
{
    if (word.length() < pattern.length())
        return false;

    size_t index = word.find(pattern);
    if (string::npos != index) {
        (*slice).clear();
        for (size_t i = 0; i < pattern.size() + index; ++i)
            (*slice).push_back(word[i]); // ������������ �������� ����� ��� ���������� ���������
    }
    else
        return false;

    return true;
}

// �������� ������� ������ word ����� ���������� ������� �� ���� ������ � ��������� ������� dict.
// ������� ��������� ������������ � wordArea.
// ������� ���������� true ��� �������� ������ � false � ��������� ������.
bool GetAreaRV(const string word, string* wordArea, const char* dict, const size_t* dictLen)
{
    bool findLetter = false;
    size_t id = 0;

    // ����� ������� ������ �������
    for (id; word[id] && !findLetter; ++id) {
        // ���������� ������� ������ ��� ���
        for (size_t j = 0; j < *dictLen; ++j) {
            if (word[id] == dict[j]) {
                findLetter = true;
                break;
            }
        }
    }

    if (findLetter) {
        (*wordArea).clear();
        (*wordArea).append(&word[id]);
    }

    return findLetter;
}

// �������� ������� ������ word ����� ���������� ������ ������� �� ���� ������
// � ��������� ������� dict1 � ������������ ������� � ��������� ������� dict2.
// ������� ��������� ������������ � wordArea.
// ������� ���������� true ��� �������� ������ � false � ��������� ������.
bool GetAreaR(const string word, string* wordArea, const char* dict1, const size_t* dict1Len,
    const char* dict2, const size_t* dict2Len)
{
    bool response = false;
    size_t id = 0;

    for (id; word[id] && !response; ++id) {
        // ���������� ������� ������ ��� ���
        for (size_t j = 0; j < *dict1Len && !response; ++j) {
            if (word[id] == dict1[j]) {
                // ���������� ��������� ������ ��� ���
                for (size_t k = 0; word[id + 1] && k < *dict2Len; ++k) {
                    if (word[id + 1] == dict2[k]) {
                        response = true;
                        break;
                    }
                }
            }
        }
    }

    if (response) {
        (*wordArea).clear();
        (*wordArea).append(&word[id + 1]);
    }

    return response;
}

// ���� ��������� ����� � ������ word, ������� �������� � ����� ������� ���������� �� ������� ��������� dict.
// ���������� true, ���� ��������� ���� �������, � ���������� id ������������ ������ ������� � ������ word
// � �������� ���������� ���������. ���� ��������� �� ���� ������� ������� ���������� false.
bool FindEndWord(const string word, const string* dict, const size_t* dictLen, size_t* id)
{
    string endWord;
    int tempId = (int)word.length(); // ������ ���������
    int size = tempId;
    bool response = false;

    // ����� ������������� ���������
    for (int i = size - 1; i >= 0; --i) {
        endWord.clear();
        endWord.append(&word[i]);

        // ���������� ��������� � ����������� �� �������
        for (size_t j = 0; j < *dictLen; ++j) {
            if (endWord.length() == dict[j].length()) {
                // ������ ��������� ������ ����������� � ��������� �������
                if (tempId > i && endWord == dict[j]) {
                    *id = tempId = i; // ��������� ������ ������ ���������
                    response = true;
                }
            }
        }
    }

    return response;
}

// ��������� ����� ������ ����� �� ��������� �������� �������.
// ������ ����� ������������ � base. ���������� true ��� �������� ������,
// ����� - false.
bool Stemmer(const string word, string* base)
{
    string RV;

    if (!GetAreaRV(word, &RV, VOWEL, &VOWLen))
        return false;

    // ��� 1: �������� ���������
    size_t id;
    if (!FindEndWord(RV, PERFECTIVEGERUND, &PEGELen, &id)) {
        if (FindEndWord(RV, REFLEXIVE, &REFLen, &id)) // ������� REFLEXIVE
            RV.resize(id);
        if (FindEndWord(RV, ADJECTIVE, &ADJLen, &id)) { // ������� ADJECTIVE
            RV.resize(id);
            if (FindEndWord(RV, PARTICIPLE, &PARLen, &id)) // ������� PARTICIPLE
                RV.resize(id);
        }
        else if (FindEndWord(RV, VERB, &VERLen, &id)) // ������� VERB
            RV.resize(id);
        else if (FindEndWord(RV, NOUN, &NOUNLen, &id)) // ������� NOUN
            RV.resize(id);
    }
    else
        RV.resize(id);

    // ��� 2: ���� RV ������������ �� � ������� ��
    if (RV[RV.size() - 1] == '�')
        RV.resize(RV.size() - 1);

    // ��� 3: ������� ��������� DERIVATIONAL
    string R1, R2, copy;
    if (GetPatternSlice(word, RV, &copy)) // �������� �������� ����� ��� ���������� ���������
        if (GetAreaR(copy, &R1, VOWEL, &VOWLen, CONSONANT, &CONLen))
            if (GetAreaR(R1, &R2, VOWEL, &VOWLen, CONSONANT, &CONLen))
                if (FindEndWord(R2, DERIVATIONAL, &DERLen, &id))
                    R2.resize(id);

    // ��� 4
    GetPatternSlice(word, R2, &copy); // �������� �������� ����� � ����������� �����������

    if (GetAreaRV(copy, &RV, VOWEL, &VOWLen)) {
        if (RV[RV.size() - 1] == '�')
            RV.resize(RV.size() - 1);
        else if (RV[RV.size() - 1] == '�' && RV[RV.size() - 2] == '�')
            RV.resize(RV.size() - 1);
        else if (FindEndWord(RV, SUPERLATIVE, &SUPLen, &id))
        {
            RV.resize(id);
            if (RV[RV.size() - 1] == '�' && RV[RV.size() - 2] == '�')
                RV.resize(RV.size() - 1);
        }
    }

    // ������������ ������ �����
    if (GetPatternSlice(word, RV, &copy)) {
        (*base).clear();
        (*base).append(copy);
    }
    else
        return false;

    return true;
}

// ������� ������� ������ ��������� subS � ������ s. ��� �������� ������
// ���������� ������ ������ ��������� ��������� � ������, � ���������
// ������ ���������� -1.
int StraightSearch(string str, string subS)
{
    for (size_t i = 0; str[i]; ++i)
        for (size_t j = 0; subS[j] == str[i + j]; ++j)
            if (!subS[j + 1]) // ���� ������ ���� ������
                return i;
    return -1;
}


// ���������� ����� �� ������ line �� ������� index, ��� ���� ���������
// �������� �� ����� � ������� ������� �����. ��� �������� ������
// ���������� true, ����� - false.
bool GetWord(int index, string line, string* str, string alphabet)
{
    bool response = true;
    (*str).clear();

    // ���������, �� ���������� �� ����� ������ � ������
    if (index != 0)
        for (size_t i = 0; alphabet[i]; ++i)
            if (alphabet[i] == line[index - 1])
                response = false;

    if (response)
        for (size_t i = index; line[i] && line[i] != ',' && line[i] != '.' && line[i] != ' '; ++i)
            (*str).push_back(line[i]);

    return response;
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "");

    string filePath;

    cout << "��������� ��������� ����� ����� � ����� ������� � �����." << endl;
    cout << "���� � �����: ";
    cin >> filePath;

    ifstream file(filePath);

    if (file.is_open()) {
        string line, word, wordBase;
        
        cout << "������� ����� ��� ������: ";
        cin >> word;

        if (Stemmer(word, &wordBase)) {
            cout << "������ ����� \"" << word << "\": " << wordBase << endl;
            if (GetAreaRV(word, &line, VOWEL, &VOWLen))
                cout << "������� ����� RV: " << line << endl;
            if (GetAreaR(word, &line, VOWEL, &VOWLen, CONSONANT, &CONLen)) {
                cout << "������� ����� R1: " << line << endl;
                if (GetAreaR(line, &line, VOWEL, &VOWLen, CONSONANT, &CONLen))
                    cout << "������� ����� R2: " << line << endl;
            }

            // ����� �� �����
            bool isOpen = false;
            ofstream outF("outWords.txt");
            isOpen = outF.is_open();
            int i = 0, res = -1;
            while (getline(file, line)) {
                res = StraightSearch(line, wordBase);
                ++i;
                if (res != -1) {
                    if (isOpen) {
                        if (GetWord(res, line, &word, ALPAHBET))
                        {
                            cout << "����� � ����� �� ������� ������� � ������ �" << i
                                << ", ������� � ������� " << res << endl << line <<endl;
                            outF << word << endl;
                        }
                    }
                }
            }
        }
        else
            cout << "�� ������� �������� ������ �����." << endl;
                
        file.close();
    }
    else
        cout << "�� ������� ������� ����." << endl;
}
